<?php return array(

    'make_deposit'     => 'Make deposit',
    'could_not_find'   => 'Make deposit',
    'amount'           => 'Amount',
    'pay'              => 'Pay',
    'currency'         => 'Currency',
    'added'            => 'Deposit added.',
    'show_paid'        => 'Show paid',
    'deposit'          => 'Deposit',
    'deposits_list'    => 'Deposits list',
    'make_new_deposit' => 'Make new deposit',
    'select_currency'  => 'Select currency',
    'paid'             => 'Paid',
    'unpaid'           => 'Unpaid',
    'id'               => 'Order ID',
    'created_at'       => 'Date',
    'status'           => 'Status',
    'deposits'         => 'All deposits',
    'user_id'          => 'User ID',
    'user_email'       => 'User email',

);