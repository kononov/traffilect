<?php return array(
    'add_new_project'            => 'Add new project',
    'added'                      => 'Project added.',
    'enabled'                    => 'Enable',
    'could_not_find'             => 'Could not find selected project',
    'date'                       => 'Date',
    'delete_project'             => 'Delete project',
    'deleted'                    => 'Project deleted.',
    'description'                => 'Description',
    'edit_project'               => 'Edit project',
    'disable'                    => 'Disable',
    'keyword'                    => 'Keyword',
    'on'                         => 'Enable',
    'off'                        => 'Disable',
    'show_enabled'               => 'Show enable',
    'status'                     => 'Status',
    'title'                      => 'Title',
    'domain'                     => 'Domain',
    'project'                    => 'Project',
    'project_list'               => 'Project list',
    'updated'                    => 'Project updated.',
    'block_robots'               => 'Block robots',
    'block_moderators'           => 'Block moderators',
    'time'                       => 'Time',
    'project_lastlog'            => 'Last project logs',
    'tip_block_robots'           => 'Detecting and block all robots automatically.',
    'tip_block_moderators'       => 'Detecting and block all moderators automatically.',
    'bad_destination'            => 'Bad traffic destination',
    'good_destination'           => 'Good traffic destination',
    'tip_bad_destination'        => 'All bad traffic will be redirection to this url',
    'tip_good_destination'       => 'All good traffic will be redirection to this url',
    'enable_destinations'        => 'Use destinations',
    'tip_enable_destinations'    => 'You can set destination urls for you bad and good traffic.',
    'enter_bad_destination_url'  => 'Enter bad destination url, with http(s)://',
    'enter_good_destination_url' => 'Enter good destination url, with http(s)://',
    'destinations'               => 'Destinations',
    'select_individual_rule'     => 'Select individual rule',
    'individual_rule'            => 'Individual rule',
    'code_for_insert' => 'For enable filtering insert this javascript code into you site:'
);