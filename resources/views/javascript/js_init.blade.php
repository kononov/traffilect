var info={
    timeOpened:new Date(),
    timezone:(new Date()).getTimezoneOffset()/60,
    pageon(){return window.location.pathname},
    previousSites(){return history.length},
    browserName(){return navigator.appName},
    browserEngine(){return navigator.product},
    browserVersion1a(){return navigator.appVersion},
    browserVersion1b(){return navigator.userAgent},
    browserLanguage(){return navigator.language},
    browserOnline(){return navigator.onLine},
    browserPlatform(){return navigator.platform},
    javaEnabled(){return navigator.javaEnabled()},
    dataCookiesEnabled(){return navigator.cookieEnabled},
    sizeScreenW(){return screen.width},
    sizeScreenH(){return screen.height},
    sizeInW(){return innerWidth},
    sizeInH(){return innerHeight},
    sizeAvailW(){return screen.availWidth},
    sizeAvailH(){return screen.availHeight},
    scrColorDepth(){return screen.colorDepth},
    scrPixelDepth(){return screen.pixelDepth},
};

xmlhttp = new XMLHttpRequest;
xmlhttp.open("GET", "//js.traffilect.com/{{ $project->id }}");
xmlhttp.setRequestHeader("timeOpened", info.timeOpened);
xmlhttp.setRequestHeader("timezone", info.timezone);
xmlhttp.setRequestHeader("pageon", info.pageon());
xmlhttp.setRequestHeader("previousSites", info.previousSites());
xmlhttp.setRequestHeader("browserName", info.browserName());
xmlhttp.setRequestHeader("browserEngine", info.browserEngine());
xmlhttp.setRequestHeader("browserVersion1a", info.browserVersion1a());
xmlhttp.setRequestHeader("browserVersion1b", info.browserVersion1b());
xmlhttp.setRequestHeader("browserLanguage", info.browserLanguage());
xmlhttp.setRequestHeader("browserOnline", info.browserOnline());
xmlhttp.setRequestHeader("browserPlatform", info.browserPlatform());
xmlhttp.setRequestHeader("javaEnabled", info.javaEnabled());
xmlhttp.setRequestHeader("dataCookiesEnabled", info.dataCookiesEnabled());
xmlhttp.setRequestHeader("sizeScreenW", info.sizeScreenW());
xmlhttp.setRequestHeader("sizeScreenH", info.sizeScreenH());
xmlhttp.setRequestHeader("sizeInW", info.sizeInW());
xmlhttp.setRequestHeader("sizeInH", info.sizeInH());
xmlhttp.setRequestHeader("sizeAvailW", info.sizeAvailW());
xmlhttp.setRequestHeader("sizeAvailH", info.sizeAvailH());
xmlhttp.setRequestHeader("scrColorDepth", info.scrColorDepth());
xmlhttp.setRequestHeader("scrPixelDepth", info.scrPixelDepth());
xmlhttp.onreadystatechange = function () {
if (xmlhttp.readyState == 4) {
    if (xmlhttp.status == 200) {
        var data = JSON.parse(xmlhttp.responseText);
        if(data.redirect) {
            location.href = data.redirect;
        }
    }
}
};
xmlhttp.send();