#!/bin/sh

export HOME=/var/tmp/
su - www-data -s /bin/sh -c "php /usr/bin/composer install --no-interaction -o -d /var/www/traffilect"
