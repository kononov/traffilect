<?php

/**
 * Here you can define all the default options globally & access via config('default.color_theme') to get default option
 */

return array(
    'color_theme'                   => 'green',
    // default color theme
    'direction'                     => 'ltr',
    // default direction; can be "ltr" or "rtl"
    'locale'                        => 'en',
    // default language
    'timezone'                      => 'America/New_York',
    // default timezone; can be anytime zone from config/timezone.php
    'notification_position'         => 'toast-bottom-right',
    // default position for notification; can be "top-right","bottom-right","top-left","bottom-left"
    'date_format'                   => 'DD-MM-YYYY',
    // default date format
    'time_format'                   => 'H:mm',
    // default time format
    'page_length'                   => '10',
    // default record per page
    'driver'                        => 'log',
    // default mail driver
    'from_address'                  => 'system@traffilect.com',
    // default mail sender email
    'from_name'                     => 'Traffilect.com',
    // default mail sender name
    'token_lifetime'                => 120,
    // default lifetime of authentication token
    'reset_password_token_lifetime' => 30,
    // default lifetime for password reset token
    'activity_log'                  => 1,
    // Activity log enabled by default, put 0 to disable
    'email_log'                     => 1,
    // Email log enabled by default, put 0 to disable
    'reset_password'                => 1,
    // Reset Password enabled by default, put 0 to disable
    'registration'                  => 1,
    // Registration/Account Creation enabled by default, put 0 to disable
    'mode'                          => 1,
    // Put 1 for live mode & 0 for test mode
    'footer_credit'                 => 'Copyright © Traffilect.com',
    'multilingual'                  => 1,
    'ip_filter'                     => 1,
    'email_template'                => 1,
    'todo'                          => 1,
    'rule'                          => 1,
    'individual_rule'               => 1,
    'deposit'                       => 1,
    'deposits'                      => 1,
    'message'                       => 1,
    'backup'                        => 1,
    'project'                       => 1,
    'project_log'                   => 1,
    'show_user_menu'                => 1,
    'show_deposit_menu'             => 1,
    'show_deposits_menu'            => 1,
    'show_todo_menu'                => 1,
    'show_rule_menu'                => 1,
    'show_individual_rule_menu'     => 1,
    'show_project_menu'             => 1,
    'show_project_log_menu'         => 1,
    'show_message_menu'             => 1,
    'show_configuration_menu'       => 1,
    'show_backup_menu'              => 1,
    'show_email_template_menu'      => 1,
    'show_email_log_menu'           => 1,
    'show_activity_log_menu'        => 1,
    'password_strength_meter'       => 1,
    'terms_and_conditions'          => 1
);
