<?php

 /**
  * Here you can define all the list options globally & access via config('lists.gender') to get lists in form of array
  */

return array(
    'gender' => array('male','female'),
);
