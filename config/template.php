<?php

 /**
  * Here you can define all the default templates globally & access via config('template.welcome-email-user') to get template category, subject or fields
  */

return array (
    'welcome-email-user' => array(
        'category' => 'user',
        'subject' => 'Welcome Email User | [COMPANY_NAME]',
        'fields' => 'NAME, EMAIL, PASSWORD, COMPANY_NAME, COMPANY_EMAIL, COMPANY_PHONE, COMPANY_WEBSITE, COMPANY_ADDRESS,CURRENT_DATE,CURRENT_DATE_TIME'
    ),
    'anniversary-email-user' => array(
        'category' => 'user',
        'subject' => 'Wish You a Very Happy Anniversary [NAME] | [COMPANY_NAME]'
    ),
    'birthday-email-user' => array(
        'category' => 'user',
        'subject' => 'Happy Birthday [NAME] | [COMPANY_NAME]'
    ),
);
