<?php

/**
 * Here you can define all the default system variables globally & access via config('system.default_role') to get default option
 */

return array(
    'default_role'             => array(                // Currently system supports two default role; one for admin & another for user
                                                        'admin' => 'admin',
                                                        'user'  => 'user'
    ),
    'default_template_field'   => array(      // Currently system supports only one template category "user" for which variables are defined
                                              'user' => 'NAME, EMAIL, COMPANY_NAME, COMPANY_EMAIL, COMPANY_PHONE, COMPANY_WEBSITE, CURRENT_DATE_TIME, CURRENT_DATE, DATE_OF_BIRTH, DATE_OF_ANNIVERSARY',
    ),
    'hidden_field'             => 'xxxxxxxxxx',
    // Hidden fields are system variables that is used to hide private configuration variables as listed below
    'private_config_variables' => array(
        'smtp_username',
        'smtp_password',
        'mailgun_username',
        'mailgun_password',
        'mailgun_secret',
        'mandrill_secret',
        'nexmo_api_key',
        'nexmo_api_secret',
        'facebook_client',
        'facebook_secret',
        'twitter_client',
        'twitter_secret',
        'github_client',
        'github_secret'
    ),
    'public_config'            => array(
        'color_theme',
        'date_format',
        'time_format',
        'notification_position',
        'direction',
        'maintenance_mode',
        'registration',
        'reset_password',
        'password_strength_meter',
        'terms_and_conditions',
        'two_factor_security',
        'social_login',
        'https',
        'mode',
        'email_verification',
        'footer_credit',
        'ip_filter'
    ),
    'social_login_providers'   => array(         // You can add more social login providers
                                                 'facebook',
                                                 'twitter',
                                                 'github'
    ),
    'upload_path'              => array(
        'logo'   => 'uploads/logo',
        'avatar' => 'uploads/avatar'
    ),
    'pagination'               => array(1, 5, 10, 25, 50, 100),
    'config_variable_list'     => array(
        'mail_drivers'           => array(
            array(
                'text'  => 'Mail',
                'value' => 'mail'
            ),
            array(
                'text'  => 'Sendmail',
                'value' => 'sendmail'
            ),
            array(
                'text'  => 'Mailgun',
                'value' => 'mailgun'
            ),
            array(
                'text'  => 'Mandrill',
                'value' => 'mandrill'
            ),
            array(
                'text'  => 'SMTP',
                'value' => 'smtp'
            ),
            array(
                'text'  => 'Log',
                'value' => 'log'
            )
        ),
        'color_themes'           => array(
            array(
                'text'  => 'Blue',
                'value' => 'blue'
            ),
            array(
                'text'  => 'Blue with Dark Sidebar',
                'value' => 'blue-dark'
            ),
            array(
                'text'  => 'Red',
                'value' => 'red'
            ),
            array(
                'text'  => 'Red with Dark Sidebar',
                'value' => 'red-dark'
            ),
            array(
                'text'  => 'Default',
                'value' => 'default'
            ),
            array(
                'text'  => 'Default with Dark Sidebar',
                'value' => 'default-dark'
            ),
            array(
                'text'  => 'Green',
                'value' => 'green'
            ),
            array(
                'text'  => 'Green with Dark Sidebar',
                'value' => 'green-dark'
            ),
            array(
                'text'  => 'Megna',
                'value' => 'megna'
            ),
            array(
                'text'  => 'Megna with Dark Sidebar',
                'value' => 'megna-dark'
            ),
            array(
                'text'  => 'Purple',
                'value' => 'purple'
            ),
            array(
                'text'  => 'Purple with Dark Sidebar',
                'value' => 'purple-dark'
            )
        ),
        'directions'             => array(
            array(
                'text'  => 'Right to Left',
                'value' => 'rtl'
            ),
            array(
                'text'  => 'Left to Right',
                'value' => 'ltr'
            )
        ),
        'date_formats'           => array(
            array(
                'text'  => date('d-m-Y'),
                'value' => 'DD-MM-YYYY'
            ),
            array(
                'text'  => date('m-d-Y'),
                'value' => 'MM-DD-YYYY'
            ),
            array(
                'text'  => date('d-M-Y'),
                'value' => 'DD-MMM-YYYY'
            ),
            array(
                'text'  => date('M-d-Y'),
                'value' => 'MMM-DD-YYYY'
            )
        ),
        'time_formats'           => array(
            array(
                'text'  => date('H:i'),
                'value' => 'H:mm'
            ),
            array(
                'text'  => date('h:i a'),
                'value' => 'h:mm a'
            )
        ),
        'notification_positions' => array(
            array(
                'text'  => 'Top Right',
                'value' => 'toast-top-right'
            ),
            array(
                'text'  => 'Top Left',
                'value' => 'toast-top-left'
            ),
            array(
                'text'  => 'Top Bottom Right',
                'value' => 'toast-bottom-right'
            ),
            array(
                'text'  => 'Top Bottom Left',
                'value' => 'toast-bottom-left'
            )
        )
    ),
    'default_permission'       => array(          // Default permissions supported by system
                                                  'access-configuration',
                                                  'list-user',
                                                  'create-user',
                                                  'edit-user',
                                                  'delete-user',
                                                  'force-reset-user-password',
                                                  'email-user',
                                                  'change-status-user',
                                                  'access-message',
                                                  'access-todo',
                                                  'access-rule',
                                                  'access-individual-rule',
                                                  'access-deposit',
                                                  'access-deposits',
                                                  'enable-login',
                                                  'access-project',
                                                  'project-log',

    )
);
