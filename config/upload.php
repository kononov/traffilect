<?php

 /**
  * Here you can define all the default upload options globally & access via config('upload.message') to get upload option for that module
  */

return array(
    'message' => array(
        'auth_required' => 1,
        'max_file_size' => 1,
        'allowed_file_extensions' => ['jpg','png','jpeg','pdf','doc','docx','xls','xlsx','txt'],
        'max_no_of_files' => 5
    )
);
