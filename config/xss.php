<?php

 /**
  * Here you can define all the routes that will be not be checked by XSS Protection middleware
  */

return array(
        'api/message/compose' => array('body'),
        'api/message/reply' => array('body'),
        'api/email-template/*' => array('body')
    );
?>
