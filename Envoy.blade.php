@servers(['dev' => 'traffilect@165.227.250.191','prod' => 'traffilect@165.227.250.191'])
@task('deploy-dev', ['on' => 'dev'])
cd /home/traffilect/dev
git fetch --all
git reset --hard origin/dev
composer install -n --no-dev --no-scripts
composer update
php artisan down
php artisan refresh
php artisan up
npm install
npm run prod
sudo chmod -R 0777 public/uploads storage bootstrap/cache
php artisan cache:clear
php artisan config:clear
php artisan geoip:update
@endtask
@task('deploy-prod', ['on' => 'prod'])
cd /home/traffilect/public_html
git fetch --all
git reset --hard origin/master
composer install -n --no-dev --no-scripts
composer update
php artisan down
php artisan migrate --force
php artisan up
npm install
npm run prod
sudo chmod -R 0777 public/uploads storage bootstrap/cache
php artisan cache:clear
php artisan config:clear
php artisan geoip:update
@endtask
