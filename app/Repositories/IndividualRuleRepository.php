<?php
namespace App\Repositories;

use App\IndividualRule;
use App\User;
use Carbon\Carbon;
use Illuminate\Validation\ValidationException;

/**
 * Class IndividualRuleRepository
 * @package App\Repositories
 */
class IndividualRuleRepository
{

    /**
     * @var IndividualRule
     */
    private $rule;

    /**
     * IndividualRuleRepository constructor.
     *
     * @param IndividualRule $rule
     */
    public function __construct(IndividualRule $rule)
    {
        $this->rule = $rule;
    }

    /**
     * @return IndividualRule
     */
    public function getQuery()
    {
        return $this->rule;
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static|static[]
     * @throws ValidationException
     */
    public function findOrFail($id)
    {
        $rule = $this->rule->with('users')->find($id);

        if (!$rule) {
            throw ValidationException::withMessages(['message' => trans('individual_rule.could_not_find')]);
        }

        return $rule;
    }

    /**
     * @param $params
     *
     * @return mixed
     */
    public function paginate($params)
    {
        $sort_by     = isset($params['sort_by']) ? $params['sort_by'] : 'created_at';
        $order       = isset($params['order']) ? $params['order'] : 'desc';
        $page_length = isset($params['page_length']) ? $params['page_length'] : config('config.page_length');
        $keyword     = isset($params['keyword']) ? $params['keyword'] : null;
        $status      = isset($params['status']) ? $params['status'] : 0;
        $start_date  = isset($params['start_date']) ? $params['start_date'] : null;
        $end_date    = isset($params['end_date']) ? $params['end_date'] : null;

        $query = $this->rule->filterByTitleOrDescription($keyword)->filterCompletedRule($status)->dateBetween([
            'start_date' => $start_date,
            'end_date'   => $end_date
        ]);

        return $query->with('users')->orderBy($sort_by, $order)->paginate($page_length);
    }

    /**
     * @param $params
     *
     * @return IndividualRule
     */
    public function create($params): IndividualRule
    {
        $rule        = $this->rule->forceCreate($this->formatParams($params));
        $users_id = isset($params['user_id']) ? $params['user_id'] : null;
        $users_id = collect($users_id)->values()->all();

        if (!User::find($users_id) || !$users_id) {
            throw ValidationException::withMessages(['message' => trans('individual_rule.user_not_found')]);
        }

        $this->AssignUser($rule, $users_id);

        return $rule;
    }

    /**
     * @param IndividualRule $rule
     * @param array          $users_id
     * @param string         $action
     */
    private function AssignUser(IndividualRule $rule, $users_id = array(), $action = 'attach')
    {
        if ($action === 'attach') {
            $rule->users()->attach($users_id);
        } else {
            $rule->users()->sync($users_id);
        }
    }

    /**
     * @param        $params
     * @param string $action
     *
     * @return array
     */
    private function formatParams($params, $action = 'create')
    {
        $formatted = [
            'title'        => isset($params['title']) ? $params['title'] : null,
            'description'  => isset($params['description']) ? $params['description'] : null,
            'country_id'   => isset($params['country_id']) ? $params['country_id'] : null,
            'keyword_name' => isset($params['keyword_name']) ? $params['keyword_name'] : null
        ];

        if ($action === 'create') {
            $formatted['user_id'] = \Auth::user()->id;
        }

        return $formatted;
    }

    /**
     * @param IndividualRule $rule
     * @param                $params
     *
     * @return IndividualRule
     */
    public function update(IndividualRule $rule, $params)
    {

        $rule->forceFill($this->formatParams($params, 'update'))->save();

        return $rule;
    }

    /**
     * @param IndividualRule $rule
     *
     * @return bool|null
     * @throws \Exception
     */
    public function delete(IndividualRule $rule)
    {
        return $rule->delete();
    }

    /**
     * @param $ids
     *
     * @return mixed
     */
    public function deleteMultiple($ids)
    {
        return $this->rule->whereIn('id', $ids)->delete();
    }

    /**
     * @param IndividualRule $rule
     *
     * @return IndividualRule
     */
    public function toggle(IndividualRule $rule)
    {
        $rule->forceFill([
            'completed_at' => (!$rule->status) ? Carbon::now() : null,
            'status'       => !$rule->status
        ])->save();

        return $rule;
    }

    /**
     * @param array $names
     *
     * @return mixed
     */
    public function listExceptName($names = array())
    {
        return $this->rule->FilterByUserId(\Auth::user()->id)->whereNotIn('title', $names)->get()->pluck('title',
            'id')->all();
    }

}