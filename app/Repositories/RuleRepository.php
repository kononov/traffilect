<?php
namespace App\Repositories;

use App\Project;
use App\Rule;
use Carbon\Carbon;
use Illuminate\Validation\ValidationException;

/**
 * Class RuleRepository
 * @package App\Repositories
 */
class RuleRepository
{

    /**
     * @var Rule
     */
    private $rule;

    /**
     * RuleRepository constructor.
     *
     * @param Rule $rule
     */
    public function __construct(Rule $rule)
    {
        $this->rule = $rule;
    }

    /**
     * @return Rule
     */
    public function getQuery()
    {
        return $this->rule;
    }

    /**
     * @param $id
     *
     * @return mixed
     * @throws ValidationException
     */
    public function findOrFail($id)
    {
        $rule = $this->rule->with('projects')->find($id);

        if (!$rule) {
            throw ValidationException::withMessages(['message' => trans('rule.could_not_find')]);
        }

        return $rule;
    }

    /**
     * @param $params
     *
     * @return mixed
     */
    public function paginate($params)
    {
        $sort_by     = isset($params['sort_by']) ? $params['sort_by'] : 'created_at';
        $order       = isset($params['order']) ? $params['order'] : 'desc';
        $page_length = isset($params['page_length']) ? $params['page_length'] : config('config.page_length');
        $keyword     = isset($params['keyword']) ? $params['keyword'] : null;
        $status      = isset($params['status']) ? $params['status'] : 0;
        $start_date  = isset($params['start_date']) ? $params['start_date'] : null;
        $end_date    = isset($params['end_date']) ? $params['end_date'] : null;

        $query = $this->rule->filterByUserId(\Auth::user()->id)->filterByTitleOrDescription($keyword)->filterCompletedRule($status)->dateBetween([
            'start_date' => $start_date,
            'end_date'   => $end_date
        ]);

        return $query->with('projects')->orderBy($sort_by, $order)->paginate($page_length);
    }

    /**
     * @param $params
     *
     * @return Rule
     */
    public function create($params): Rule
    {
        $rule        = $this->rule->forceCreate($this->formatParams($params));
        $projects_id = isset($params['project_id']) ? $params['project_id'] : null;

        if ($projects_id) {
            $this->AssignProject($rule, $projects_id);
        }

        return $rule;
    }

    /**
     * @param Rule   $rule
     * @param        $projects_id
     * @param string $action
     */
    private function AssignProject(Rule $rule, $projects_id = array(), $action = 'attach')
    {

        if ($action === 'attach') {
            foreach ($projects_id as $project_id) {
                if ($project = Project::find($project_id)->where('id', \Auth::user()->id)->first()) {
                    $rule->projects()->attach($project_id);
                }
            }
        } else {
            $rule->projects()->sync($projects_id);
        }
    }

    /**
     * @param        $params
     * @param string $action
     *
     * @return array
     */
    private function formatParams($params, $action = 'create')
    {
        $formatted = [
            'title'        => isset($params['title']) ? $params['title'] : null,
            'description'  => isset($params['description']) ? $params['description'] : null,
            'country_id'   => isset($params['country_id']) ? $params['country_id'] : null,
            'keyword_name' => isset($params['keyword_name']) ? $params['keyword_name'] : null
        ];

        if ($action === 'create') {
            $formatted['user_id'] = \Auth::user()->id;
        }

        return $formatted;
    }

    /**
     * @param Rule $rule
     * @param      $params
     *
     * @return Rule
     */
    public function update(Rule $rule, $params)
    {

        $rule->forceFill($this->formatParams($params, 'update'))->save();

        return $rule;
    }

    /**
     * @param Rule $rule
     *
     * @return bool|null
     * @throws \Exception
     */
    public function delete(Rule $rule)
    {
        return $rule->delete();
    }

    /**
     * @param $ids
     *
     * @return mixed
     */
    public function deleteMultiple($ids)
    {
        return $this->rule->whereIn('id', $ids)->delete();
    }

    /**
     * @param Rule $rule
     *
     * @return Rule
     */
    public function toggle(Rule $rule)
    {
        $rule->forceFill([
            'completed_at' => (!$rule->status) ? Carbon::now() : null,
            'status'       => !$rule->status
        ])->save();

        return $rule;
    }

}