<?php
namespace App\Repositories;

use App\Deposit;
use Carbon\Carbon;
use Illuminate\Validation\ValidationException;

/**
 * Class DepositRepository
 * @package App\Repositories
 */
class DepositRepository
{
    private $deposit;

    public function __construct(Deposit $deposit)
    {
        $this->deposit = $deposit;
    }


    public function getQuery()
    {
        return $this->deposit;
    }


    public function findOrFail($id)
    {
        $deposit = $this->deposit->find($id);

        if (! $deposit) {
            throw ValidationException::withMessages(['message' => trans('deposit.could_not_find')]);
        }

        return $deposit;
    }

    public function paginate($params, $userFilter = true)
    {
        $sort_by     = isset($params['sort_by']) ? $params['sort_by'] : 'created_at';
        $order      = isset($params['order']) ? $params['order'] : 'desc';
        $page_length = isset($params['page_length']) ? $params['page_length'] : config('config.page_length');
        $status     = isset($params['status']) ? $params['status'] : 0;
        $start_date = isset($params['start_date']) ? $params['start_date'] : null;
        $end_date   = isset($params['end_date']) ? $params['end_date'] : null;


        $userFilter ? $query = $this->deposit->filterByUserId(\Auth::user()->id) : $query = $this->deposit->with('user');

        $query->filterCompletedDeposit($status)->dateBetween([
            'start_date' => $start_date,
            'end_date' => $end_date
        ]);

        return $query->with('currency')->orderBy($sort_by, $order)->paginate($page_length);
    }


    public function create($params)
    {
        return $this->deposit->forceCreate($this->formatParams($params));
    }


    private function formatParams($params, $action = 'create')
    {
        $formatted = [
            'amount'       => isset($params['amount']) ? $params['amount'] : null,
            'currency_id' => isset($params['currency_id']) ? $params['currency_id'] : null
        ];

        if ($action === 'create') {
            $formatted['user_id'] = \Auth::user()->id;
        }

        return $formatted;
    }

    public function toggle(Deposit $deposit)
    {
        $deposit->forceFill([
            'completed_at' => (! $deposit->status) ? Carbon::now() : null,
            'status'       => ! $deposit->status
        ])->save();

        return $deposit;
    }
}
