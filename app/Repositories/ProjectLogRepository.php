<?php

namespace App\Repositories;

use App\ProjectLog;

class ProjectLogRepository
{

    /**
     * @var ProjectLog
     */
    private $project_log;

    /**
     * ProjectLogRepository constructor.
     *
     * @param ProjectLog $project_log
     */
    public function __construct(ProjectLog $project_log)
    {
        $this->project_log = $project_log;
    }

    /**
     * @return ProjectLog
     */
    public function getQuery()
    {
        return $this->project_log;
    }

    /**
     * @param $id
     *
     * @return mixed
     * @throws ValidationException
     */
    public function findOrFail($id)
    {
        $project_log = $this->project_log->find($id);

        if (!$project_log) {
            throw ValidationException::withMessages(['message' => trans('project_log.could_not_find')]);
        }

        return $project_log;
    }

    /**
     * @param $params
     *
     * @return mixed
     */
    public function paginate($params)
    {
        $sort_by     = isset($params['sort_by']) ? $params['sort_by'] : 'created_at';
        $order       = isset($params['order']) ? $params['order'] : 'desc';
        $page_length = isset($params['page_length']) ? $params['page_length'] : config('config.page_length');
        $keyword     = isset($params['keyword']) ? $params['keyword'] : null;
        $start_date  = isset($params['start_date']) ? $params['start_date'] : null;
        $end_date    = isset($params['end_date']) ? $params['end_date'] : null;
        $project_id  = isset($params['project_id']) ? $params['project_id'] : 0;

        $query = $this->project_log->with('project')->FilterByUserId(\Auth::user()->id)->FilterByProjectId($project_id)->FilterByKeyword($keyword)->dateBetween([
            'start_date' => $start_date,
            'end_date'   => $end_date
        ]);

        return $query->orderBy($sort_by, $order)->paginate($page_length);
    }

    /**
     * @param $params
     *
     * @return mixed
     */
    public function record($params)
    {
        return $this->project_log->forceCreate($this->formatParams($params));
    }

    /**
     * @param $params
     *
     * @return array
     */
    private function formatParams($params)
    {
        $formatted = [
            'project_id'         => isset($params['project_id']) ? $params['project_id'] : null,
            'user_id'            => isset($params['user_id']) ? $params['user_id'] : null,
            'referer'            => isset($params['referer']) ? $params['referer'] : null,
            'is_bot'             => isset($params['is_bot']) ? $params['is_bot'] : 0,
            'is_moderator'       => isset($params['is_moderator']) ? $params['is_moderator'] : 0,
            'iso_country_code'   => isset($params['iso_country_code']) ? $params['iso_country_code'] : null,
            'ip_address'         => isset($params['ip_address']) ? $params['ip_address'] : null,
            'keyword'            => isset($params['keyword']) ? $params['keyword'] : null,
            'user_agent'         => isset($params['user_agent']) ? $params['user_agent'] : null,
            'browser_locale'     => isset($params['browser_locale']) ? $params['browser_locale'] : null,
            'browserName'        => isset($params['browserName']) ? $params['browserName'] : null,
            'dataCookiesEnabled' => isset($params['dataCookiesEnabled']) ? 1 : 0,
            'browserVersion1a'   => isset($params['browserVersion1a']) ? $params['browserVersion1a'] : null,
            'scrPixelDepth'      => isset($params['scrPixelDepth']) ? $params['scrPixelDepth'] : 0,
            'javaEnabled'        => isset($params['javaEnabled']) ? 1 : 0,
            'sizeScreenW'        => isset($params['sizeScreenW']) ? $params['sizeScreenW'] : 0,
            'browserOnline'      => isset($params['browserOnline']) ? 1 : 0,
            'timeOpened'         => isset($params['timeOpened']) ? $params['timeOpened'] : null,
            'browserPlatform'    => isset($params['browserPlatform']) ? $params['browserPlatform'] : null,
            'timezone_browser'   => isset($params['timezone_browser']) ? $params['timezone_browser'] : null,
            'sizeAvailH'         => isset($params['sizeAvailH']) ? $params['sizeAvailH'] : 0,
            'sizeInW'            => isset($params['sizeInW']) ? $params['sizeInW'] : 0,
            'sizeInH'            => isset($params['sizeInH']) ? $params['sizeInH'] : 0,
            'sizeScreenH'        => isset($params['sizeScreenH']) ? $params['sizeScreenH'] : 0,
            'pageon'             => isset($params['pageon']) ? $params['pageon'] : null,
            'browserVersion1b'   => isset($params['browserVersion1b']) ? $params['browserVersion1b'] : null,
            'scrColorDepth'      => isset($params['scrColorDepth']) ? $params['scrColorDepth'] : 0,
            'previousSites'      => isset($params['previousSites']) ? $params['previousSites'] : 0,
            'sizeAvailW'         => isset($params['sizeAvailW']) ? $params['sizeAvailW'] : 0,
            'browserEngine'      => isset($params['browserEngine']) ? $params['browserEngine'] : null,
            'country_name'       => isset($params['country_name']) ? $params['country_name'] : null,
            'city'               => isset($params['city']) ? $params['city'] : null,
            'state'              => isset($params['state']) ? $params['state'] : null,
            'state_name'         => isset($params['state_name']) ? $params['state_name'] : null,
            'lat'                => isset($params['lat']) ? $params['lat'] : 0,
            'long'               => isset($params['long']) ? $params['long'] : 0,
            'timezone_ip'        => isset($params['timezone_ip']) ? $params['timezone_ip'] : null,
            'continent'          => isset($params['continent']) ? $params['continent'] : null,
            'currency'           => isset($params['currency']) ? $params['currency'] : null,
        ];

        return $formatted;
    }

}