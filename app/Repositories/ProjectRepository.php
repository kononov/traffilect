<?php

namespace App\Repositories;

use App\Project;
use Carbon\Carbon;
use Illuminate\Validation\ValidationException;

/**
 * Class ProjectRepository
 * @package App\Repositories
 */
class ProjectRepository
{

    /**
     * @var Project
     */
    private $project;

    /**
     * ProjectRepository constructor.
     *
     * @param Project $project
     */
    public function __construct(Project $project)
    {
        $this->project = $project;
    }

    /**
     * @return Project
     */
    public function getQuery()
    {
        return $this->project;
    }

    /**
     * @param $id
     *
     * @return mixed
     * @throws ValidationException
     */
    public function findOrFail($id)
    {
        $project = $this->project->with('individual_rule')->find($id);

        if (!$project) {
            throw ValidationException::withMessages(['message' => trans('project.could_not_find')]);
        }

        return $project;
    }

    /**
     * @param $id
     *
     * @return bool|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static|static[]
     */
    public function findOrFailWithAllRules($id)
    {
        $project = $this->project->with(['individual_rule', 'rules', 'user'])->find($id);

        if (!$project) {
            return false;
        }

        return $project;
    }

    /**
     * @param $domain
     *
     * @return bool
     */
    public function findOrFailByDomain($domain)
    {
        $project = $this->project->where('domain', $domain)->first();

        if (!$project) {
            return false;
        }

        return $project;
    }

    /**
     * @param $params
     *
     * @return mixed
     */
    public function paginate($params)
    {
        $sort_by     = isset($params['sort_by']) ? $params['sort_by'] : 'created_at';
        $order       = isset($params['order']) ? $params['order'] : 'desc';
        $page_length = isset($params['page_length']) ? $params['page_length'] : config('config.page_length');
        $keyword     = isset($params['keyword']) ? $params['keyword'] : null;
        $status      = isset($params['status']) ? $params['status'] : 0;
        $start_date  = isset($params['start_date']) ? $params['start_date'] : null;
        $end_date    = isset($params['end_date']) ? $params['end_date'] : null;

        $query = $this->project->filterByUserId(\Auth::user()->id)->filterByTitleOrDescription($keyword)->filterCompletedProject($status)->dateBetween([
            'start_date' => $start_date,
            'end_date'   => $end_date
        ]);

        return $query->orderBy($sort_by, $order)->paginate($page_length);
    }

    /**
     * @param $params
     *
     * @return mixed
     */
    public function create($params)
    {
        return $this->project->forceCreate($this->formatParams($params));
    }

    /**
     * @param        $params
     * @param string $action
     *
     * @return array
     */
    private function formatParams($params, $action = 'create')
    {
        $formatted = [
            'title'              => isset($params['title']) ? $params['title'] : null,
            'description'        => isset($params['description']) ? $params['description'] : null,
            'block_moderators'   => isset($params['block_moderators']) ? $params['block_moderators'] : 0,
            'block_robots'       => isset($params['block_robots']) ? $params['block_robots'] : 0,
            'domain'             => getDomainFromString($params['domain']),
            'bad_destination'    => isset($params['bad_destination']) ? $params['bad_destination'] : null,
            'good_destination'   => isset($params['good_destination']) ? $params['good_destination'] : null,
            'destinations'       => isset($params['destinations']) ? $params['destinations'] : 0,
            'individual_rule_id' => isset($params['individual_rule_id']) && intval($params['individual_rule_id']) ? $params['individual_rule_id'] : null,
        ];

        if ($action === 'create') {
            $formatted['user_id'] = \Auth::user()->id;
        }

        return $formatted;
    }

    /**
     * @param Project $project
     * @param         $params
     *
     * @return Project
     */
    public function update(Project $project, $params)
    {

        $project->forceFill($this->formatParams($params, 'update'))->save();

        return $project;
    }

    /**
     * @param Project $project
     *
     * @return bool|null
     * @throws \Exception
     */
    public function delete(Project $project)
    {
        return $project->delete();
    }

    /**
     * @param $ids
     *
     * @return mixed
     */
    public function deleteMultiple($ids)
    {
        return $this->project->whereIn('id', $ids)->delete();
    }

    /**
     * @param Project $project
     *
     * @return Project
     */
    public function toggle(Project $project)
    {
        $project->forceFill([
            'completed_at' => (!$project->status) ? Carbon::now() : null,
            'status'       => !$project->status
        ])->save();

        return $project;
    }

    /**
     * @param array $names
     *
     * @return mixed
     */
    public function listExceptName($names = array())
    {
        return $this->project->FilterByUserId(\Auth::user()->id)->whereNotIn('title', $names)->get()->pluck('title',
            'id')->all();
    }

    /**
     * @param array $names
     *
     * @return mixed
     */
    public function listExceptNameAllUsers($names = array())
    {
        return $this->project->whereNotIn('title', $names)->get()->pluck('title', 'id')->all();
    }

}