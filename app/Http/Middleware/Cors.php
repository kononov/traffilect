<?php

namespace App\Http\Middleware;

use Closure;

class Cors
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request)
            ->header('Access-Control-Allow-Origin', '*')
            ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
            ->header('Access-Control-Allow-Headers', 'Content-Type, Authorization, X-Requested-With, timeOpened, timezone, pageon, Referer, previousSites, browserName, browserEngine, browserVersion1a, browserVersion1b, browserLanguage, browserOnline, browserPlatform, javaEnabled, dataCookiesEnabled, sizeScreenW, sizeScreenH, sizeInW, sizeInH, sizeAvailW, sizeAvailH, scrColorDepth, scrPixelDepth');
    }
}
