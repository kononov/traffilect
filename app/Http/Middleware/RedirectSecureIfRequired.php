<?php

namespace App\Http\Middleware;

use Closure;

class RedirectSecureIfRequired
{
    /**
     * Used to redirect to secure site if in production mode
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (\App::environment('production') && config('config.https')) {
            \Request::setTrustedProxies([$request->getClientIp()]);

            if (!$request->isSecure()) {
                return redirect()->secure($request->getRequestUri());
            }
        }

        return $next($request);
    }
}
