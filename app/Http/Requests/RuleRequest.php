<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class RuleRequest
 * @package App\Http\Requests
 */
class RuleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'        => 'required|max:30',
            'project_id'   => 'required|array',
            'description'  => 'max:200',
            'country_id'   => 'array',
            'keyword_name' => 'array',
        ];
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return [
            'title'        => trans('rule.title'),
            'description'  => trans('rule.description'),
            'project_id'   => trans('rule.project'),
            'country_id'   => trans('rule.country'),
            'keyword_name' => trans('rule.keyword'),
        ];
    }
}
