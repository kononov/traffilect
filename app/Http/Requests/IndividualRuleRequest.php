<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class IndividualRuleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'        => 'required|max:30',
            'user_id'      => 'required|array',
            'description'  => 'max:200',
            'country_id'   => 'array',
            'keyword_name' => 'array',
        ];
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return [
            'title'        => trans('individual_rule.title'),
            'description'  => trans('individual_rule.description'),
            'user_id'      => trans('individual_rule.user'),
            'country_id'   => trans('individual_rule.country'),
            'keyword_name' => trans('individual_rule.keyword'),
        ];
    }
}
