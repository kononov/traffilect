<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class DepositRequest
 * @package App\Http\Requests
 */
class DepositRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'currency_id' => 'required|exists:deposit_currencies,id',
            'amount'   => 'required|integer|min:0.01',
        ];
    }

    /**
     * @return array
     */
    public function attributes(){
        return [
            'currency_id' => trans('deposit.currency'),
            'amount' => trans('deposit.amount')
        ];
    }

}
