<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'title'              => 'required|max:30',
            'description'        => 'max:200',
            'domain'             => 'required|regex:/^(?!:\/\/)(?=.{1,255}$)((.{1,63}\.){1,127}(?![0-9]*$)[a-z0-9-]+\.?)$/i|max:100|unique:projects,domain,' . $this->id,
            'block_robots'       => 'integer|digits_between: 0,1',
            'block_moderators'   => 'integer|digits_between: 0,1',
            'bad_destination'    => 'url|max:200',
            'good_destination'   => 'url|max:200',
            'destinations'       => 'integer|digits_between: 0,1',
            'individual_rule_id' => 'integer|exists:individual_rules,id|nullable'
        ];
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return [
            'title'              => trans('project.title'),
            'domain'             => trans('project.domain'),
            'description'        => trans('project.description'),
            'block_robots'       => trans('project.block_robots'),
            'block_moderators'   => trans('project.block_moderators'),
            'bad_destination'    => trans('project.bad_destination'),
            'good_destination'   => trans('project.good_destination'),
            'destinations'       => trans('project.destinations'),
            'individual_rule_id' => trans('project.individual_rule'),
        ];
    }
}
