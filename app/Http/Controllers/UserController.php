<?php

namespace App\Http\Controllers;

use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\UserEmailRequest;
use App\Http\Requests\UserProfileRequest;
use App\Repositories\ActivityLogRepository;
use App\Repositories\RoleRepository;
use App\Repositories\UserRepository;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected $request;
    protected $repo;
    protected $activity;
    protected $role;
    protected $module = 'user';

    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */

    public function __construct(Request $request, UserRepository $repo, ActivityLogRepository $activity, RoleRepository $role)
    {
        $this->request = $request;
        $this->repo = $repo;
        $this->activity = $activity;
        $this->role = $role;

        $this->middleware('prohibited.test.mode')->only(['forceResetPassword','destroy','uploadAvatar','removeAvatar']);
    }

    /**
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function preRequisite()
    {
        $this->authorize('create', User::class);

        $countries = generateNormalSelectOption(config('country'));
        $genders = generateTranslatedSelectOption(config('lists.gender'));
        $roles = generateSelectOption($this->role->listExceptName([config('system.default_role.admin')]));

        return $this->success(compact('countries', 'roles', 'genders'));
    }

    /**
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */

    public function index()
    {
        $this->authorize('view', User::class);

        return $this->repo->paginate($this->request->all());
    }

    /**
     * @param RegisterRequest $request
     *
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */

    public function store(RegisterRequest $request)
    {
        $this->authorize('create', User::class);

        $user = $this->repo->create($this->request->all());

        $this->activity->record([
            'module'    => $this->module,
            'module_id' => $user->id,
            'activity'  => 'created'
        ]);

        return $this->success(['message' => trans('user.added')]);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static|static[]
     * @throws \Illuminate\Validation\ValidationException
     */

    public function detail()
    {
        return $this->repo->findOrFail(\Auth::user()->id);
    }

    /**
     * @param $id
     *
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Illuminate\Validation\ValidationException
     */

    public function show($id)
    {
        $this->authorize('view', User::class);

        $user = $this->repo->findOrFail($id);

        $selected_roles = generateSelectOption($user->roles()->pluck('name', 'id')->all());
        
        $roles = $user->roles()->pluck('id')->all();

        return $this->success(compact('user', 'selected_roles', 'roles'));
    }

    /**
     * @param UserProfileRequest $request
     * @param                    $id
     *
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Illuminate\Validation\ValidationException
     */

    public function update(UserProfileRequest $request, $id)
    {
        $user = $this->repo->findOrFail($id);

        $this->authorize('update', $user);

        $user = $this->repo->update($user, $this->request->all());

        return $this->success(['message' => trans('user.profile_updated')]);
    }

    /**
     * @param $id
     *
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Illuminate\Validation\ValidationException
     */

    public function updateStatus($id)
    {
        $user = $this->repo->findOrFail($id);

        $this->authorize('update', $user);

        $this->repo->status($user, request('status'));

        $this->activity->record([
            'module' => $this->module,
            'module_id' => $user->id,
            'activity' => 'updated'
        ]);

        return $this->success(['message' => trans('user.profile_updated')]);
    }

    /**
     * @param UserProfileRequest $request
     * @param                    $id
     *
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Illuminate\Validation\ValidationException
     */

    public function updateContact(UserProfileRequest $request, $id)
    {
        $user = $this->repo->findOrFail($id);

        $this->authorize('update', $user);

        $user = $this->repo->update($user, $this->request->all());

        $this->activity->record([
            'module' => $this->module,
            'module_id' => $user->id,
            'activity' => 'updated'
        ]);

        return $this->success(['message' => trans('user.profile_updated')]);
    }

    /**
     * @param $id
     *
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Illuminate\Validation\ValidationException
     */

    public function updateSocial($id)
    {
        $user = $this->repo->findOrFail($id);

        $this->authorize('update', $user);

        $user = $this->repo->update($user, $this->request->all());

        $this->activity->record([
            'module' => $this->module,
            'module_id' => $user->id,
            'activity' => 'updated'
        ]);

        return $this->success(['message' => trans('user.profile_updated')]);
    }

    /**
     * @param ChangePasswordRequest $request
     * @param                       $id
     *
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Illuminate\Validation\ValidationException
     */

    public function forceResetPassword(ChangePasswordRequest $request, $id)
    {
        $user = $this->repo->findOrFail($id);

        $this->authorize('forceResetUserPassword', $user);

        $user = $this->repo->forceResetPassword($user, request('new_password'));

        $this->activity->record([
            'module' => $this->module,
            'module_id' => $user->id,
            'activity' => 'updated'
        ]);

        return $this->success(['message' => trans('passwords.change')]);
    }

    /**
     * @param UserEmailRequest $request
     * @param                  $id
     *
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Illuminate\Validation\ValidationException
     */

    public function sendEmail(UserEmailRequest $request, $id)
    {
        $user = $this->repo->findOrFail($id);

        $this->authorize('email', $user);

        $this->repo->sendEmail($user, $this->request->all());

        $this->activity->record([
            'module' => $this->module,
            'module_id' => $user->id,
            'sub_module' => 'mail',
            'activity' => 'sent'
        ]);

        return $this->success(['message' => trans('template.mail_sent')]);
    }

    /**
     * Used to update User Profile
     * @post ("/api/user/profile/update")
     * @param ({
     *      @Parameter("first_name", type="string", required="true", description="First Name of User"),
     *      @Parameter("last_name", type="string", required="true", description="Last Name of User"),
     *      @Parameter("gender", type="string", required="true", description="Gender of User"),
     *      @Parameter("date_of_birth", type="date", required="optional", description="Date of Birth of User"),
     *      @Parameter("date_of_anniversary", type="date", required="optional", description="Date of Anniversary of User"),
     *      @Parameter("facebook_profile", type="string", required="optional", description="Facebook Profile of User"),
     *      @Parameter("twitter_profile", type="string", required="optional", description="Twitter Profile of User"),
     *      @Parameter("linkedin_profile", type="string", required="optional", description="Linked Profile of User"),
     *      @Parameter("google_plus_profile", type="string", required="optional", description="Google Plus Profile of User"),
     * })
     * @return Response
     */

    public function updateProfile(UserProfileRequest $request)
    {
        $auth_user = \Auth::user();

        $user = $this->repo->update($auth_user, $this->request->all());

        $this->activity->record([
            'module' => $this->module,
            'module_id' => $user->id,
            'sub_module' => 'profile',
            'activity' => 'updated'
        ]);

        return $this->success(['message' => trans('user.profile_updated')]);
    }

    /**
     * @param $id
     *
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function uploadAvatar($id)
    {
        $user = $this->repo->findOrFail($id);

        $this->authorize('avatar', $user);

        $image_path = config('system.upload_path.avatar').'/';

        $profile = $user->Profile;
        $image = $profile->avatar;

        if ($image && \File::exists($image)) {
            \File::delete($image);
        }

        $extension = request()->file('image')->getClientOriginalExtension();
        $filename = uniqid();
        $file = request()->file('image')->move($image_path, $filename.".".$extension);
        $img = \Image::make($image_path.$filename.".".$extension);
        $img->resize(200, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img->save($image_path.$filename.".".$extension);

        $profile->avatar = $image_path.$filename.".".$extension;
        $profile->save();

        $this->activity->record([
            'module' => $this->module,
            'module_id' => $user->id,
            'sub_module' => 'avatar',
            'activity' => 'uploaded'
        ]);

        return $this->success(['message' => trans('user.avatar_uploaded'),'image' => $image_path.$filename.".".$extension]);
    }

    /**
     * @param $id
     *
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function removeAvatar($id)
    {
        $user = $this->repo->findOrFail($id);

        $this->authorize('avatar', $user);

        $image_path = config('system.upload_path.avatar').'/';

        $profile = $user->Profile;
        $image = $profile->avatar;

        if (!$image) {
            return $this->error(['message' => trans('user.no_avatar_uploaded')]);
        }

        if (\File::exists($image)) {
            \File::delete($image);
        }

        $profile->avatar = null;
        $profile->save();

        $this->activity->record([
            'module' => $this->module,
            'module_id' => $user->id,
            'sub_module' => 'avatar',
            'activity' => 'removed'
        ]);

        return $this->success(['message' => trans('user.avatar_removed')]);
    }

    /**
     * @param $id
     *
     * @return Response
     * @throws \Exception
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Illuminate\Validation\ValidationException
     */

    public function destroy($id)
    {
        $user = $this->repo->findOrFail($id);

        $this->authorize('delete', $user);

        $this->activity->record([
            'module' => $this->module,
            'module_id' => $user->id,
            'activity' => 'deleted'
        ]);

        $this->repo->delete($user);

        return $this->success(['message' => trans('user.deleted')]);
    }
}
