<?php

namespace App\Http\Controllers;

use App\Deposit;
use App\DepositCurrency;
use App\Http\Requests\DepositRequest;
use App\Repositories\ActivityLogRepository;
use App\Repositories\DepositRepository;
use Illuminate\Http\Request;
use Kevupton\LaravelCoinpayments\Exceptions\IpnIncompleteException;
use Kevupton\LaravelCoinpayments\Models\Ipn;
use Kevupton\LaravelCoinpayments\Models\Transaction;

/**
 * Class DepositController
 * @package App\Http\Controllers
 */
class DepositController extends Controller
{

    const ITEM_CURRENCY = 'BTC';
    const ITEM_PRICE    = 0.01;

    /**
     * @var string
     */
    protected $module = 'deposit';

    /**
     * @var DepositRequest
     */
    private $request;

    /**
     * @var DepositRepository
     */
    private $repo;

    /**
     * @var ActivityLogRepository
     */
    protected $activity;

    /**
     * DepositController constructor.
     *
     * @param Request               $request
     * @param DepositRepository     $repo
     * @param ActivityLogRepository $activity
     */
    public function __construct(Request $request, DepositRepository $repo, ActivityLogRepository $activity)
    {
        $this->request  = $request;
        $this->repo     = $repo;
        $this->activity = $activity;

        $this->middleware('feature.available:deposit');
    }

    /**
     * @return mixed
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        $this->authorize('view', Deposit::class);

        return $this->repo->paginate($this->request->all());
    }

    /**
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function listAllDeposits()
    {
        $this->authorize('access-deposits', Deposit::class);

        return $this->repo->paginate($this->request->all(), false);
    }


    /**
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function preRequisite()
    {
        $this->authorize('create', Deposit::class);

        $currencies = DepositCurrency::all();

        return $this->success(['currencies' => $currencies]);
    }

    /**
     * @param DepositRequest $request
     *
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(DepositRequest $request)
    {

        $this->authorize('create', Deposit::class);

        $deposit = $this->repo->create($this->request->all());

        $this->activity->record([
            'module'   => $this->module,
            'module_id' => $deposit->id,
            'activity' => 'added'
        ]);

        return $this->success(['message' => trans('deposit.added'), 'gateway' => $this->purchaseItems($deposit)]);
    }

    /**
     * @param Deposit $deposit
     *
     * @return array
     */
    public function purchaseItems(Deposit $deposit)
    {

        $amount   = $deposit->getAttribute('amount');
        $currency = DepositCurrency::find($deposit->getAttribute('currency'))->first();

        /*
         * Calculate the price of the item (qty * ppu)
         */
        $cost = $amount * self::ITEM_PRICE;

        /** @var Transaction $transaction */
        $transaction = \Coinpayments::createTransactionSimple($cost, self::ITEM_CURRENCY, $currency->name);

        return ['transaction' => $transaction];
    }

    /**
     * @param Deposit $deposit
     *
     * @return array
     */
    public function donation (Deposit $deposit)
    {

        $amount   = $deposit->getAttribute('amount');
        $currency = DepositCurrency::find($deposit->getAttribute('currency'))->first();

        /*
         * Here we are donating the exact amount that they specify.
         * So we use the same currency in and out, with the same amount value.
         */
        $transaction = \Coinpayments::createTransactionSimple($amount, $currency, $currency->name);

        return ['transaction' => $transaction];
    }

    public function validateIpn (DepositRequest $request)
    {
        try {
            /** @var Ipn $ipn */
            $ipn = \Coinpayments::validateIPNRequest($request);

            // if the ipn came from the API side of coinpayments merchant
            if ($ipn->isApi()) {

                /*
                 * If it makes it into here then the payment is complete.
                 * So do whatever you want once the completed
                 */

                // do something here
                // Payment::find($ipn->txn_id);
            }
        }
        catch (IpnIncompleteException $e) {
            $ipn = $e->getIpn();
            /*
             * Can do something here with the IPN model if desired.
             */
        }
    }
}
