<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProjectRequest;
use App\Project;
use App\Repositories\ActivityLogRepository;
use App\Repositories\IndividualRuleRepository;
use App\Repositories\ProjectRepository;
use App\Repositories\UserRepository;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use JWTAuth;

class ProjectController extends Controller
{
    /**
     * @var string
     */
    protected $module = 'project';

    /**
     * @var Request
     */
    private $request;

    /**
     * @var ProjectRepository
     */
    private $repo;

    /**
     * @var ActivityLogRepository
     */
    protected $activity;

    /**
     * @var UserRepository
     */
    protected $user;

    /**
     * @var IndividualRuleRepository
     */
    protected $rule;

    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */

    public function __construct(
        Request $request,
        ProjectRepository $repo,
        ActivityLogRepository $activity,
        UserRepository $user,
        IndividualRuleRepository $rule
    ) {
        $this->request  = $request;
        $this->repo     = $repo;
        $this->activity = $activity;
        $this->user     = $user;
        $this->rule     = $rule;

        $this->middleware('feature.available:project');
    }

    /**
     * @return mixed
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */

    public function index()
    {
        $this->authorize('view', Project::class);

        return $this->repo->paginate($this->request->all());
    }

    /**
     * @return JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function preRequisite(): JsonResponse
    {
        $this->authorize('create', Project::class);

        $individual_rules = generateSelectOption(User::find(\Auth::user()->id)->individual_rules()->get()->pluck('title',
            'id'));

        return $this->success(['individual_rules' => $individual_rules]);
    }

    /**
     * @param ProjectRequest $request
     *
     * @return \App\Http\Controllers\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(ProjectRequest $request)
    {
        $this->authorize('create', Project::class);

        $project = $this->repo->create($this->request->all());

        $this->activity->record([
            'module'    => $this->module,
            'module_id' => $project->id,
            'activity'  => 'added'
        ]);

        return $this->success(['message' => trans('project.added')]);
    }

    /**
     * @param $id
     *
     * @return mixed
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function show($id)
    {
        $project = $this->repo->findOrFail($id);

        $this->authorize('show', $project);

        $individual_rules = generateSelectOption($this->rule->listExceptName());

        return $this->success(compact('project', 'individual_rules'));
    }

    /**
     * @param $id
     *
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function toggleStatus($id)
    {
        $project = $this->repo->findOrFail($id);

        $this->authorize('update', $project);

        $project = $this->repo->toggle($project);

        $this->activity->record([
            'module'    => $this->module,
            'module_id' => $project->id,
            'activity'  => 'updated'
        ]);

        return $this->success(['message' => trans('project.updated'), 'project' => $project]);
    }

    /**
     * @param                $id
     * @param ProjectRequest $request
     *
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update($id, ProjectRequest $request)
    {
        $project = $this->repo->findOrFail($id);

        $this->authorize('update', $project);

        $project = $this->repo->update($project, $this->request->all());

        $this->activity->record([
            'module'    => $this->module,
            'module_id' => $project->id,
            'activity'  => 'updated'
        ]);

        return $this->success(['message' => trans('project.updated')]);
    }

    /**
     * @param $id
     *
     * @return Response
     * @throws \Exception
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function destroy($id)
    {
        $project = $this->repo->findOrFail($id);

        $this->authorize('delete', $project);

        $this->activity->record([
            'module'    => $this->module,
            'module_id' => $project->id,
            'activity'  => 'deleted'
        ]);

        $this->repo->delete($project);

        return $this->success(['message' => trans('project.deleted')]);
    }
}
