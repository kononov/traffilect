<?php

namespace App\Http\Controllers;

use App\Http\Requests\IndividualRuleRequest;
use App\IndividualRule;
use App\Repositories\ActivityLogRepository;
use App\Repositories\IndividualRuleRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;

class IndividualRuleController extends Controller
{
    /**
     * @var string
     */
    protected $module = 'individual-rule';

    /**
     * @var Request
     */
    private $request;

    /**
     * @var IndividualRuleRepository
     */
    private $repo;

    /**
     * @var ActivityLogRepository
     */
    protected $activity;

    /**
     * @var UserRepository
     */
    protected $user;

    /**
     * IndividualRuleController constructor.
     *
     * @param Request                  $request
     * @param IndividualRuleRepository $repo
     * @param ActivityLogRepository    $activity
     * @param UserRepository           $user
     */
    public function __construct(Request $request, IndividualRuleRepository $repo, ActivityLogRepository $activity, UserRepository $user)
    {
        $this->request  = $request;
        $this->repo     = $repo;
        $this->activity = $activity;
        $this->user = $user;

        $this->middleware('feature.available:individual_rule');
    }

    /**
     * @return mixed
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */

    public function index()
    {
        $this->authorize('view', IndividualRule::class);

        return $this->repo->paginate($this->request->all());
    }

    /**
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function preRequisite()
    {
        $this->authorize('create', IndividualRule::class);

        $countries = generateNormalSelectOption(config('country'));
        $users = generateSelectOption($this->user->getUsersIdAndEmail());

        return $this->success(compact('countries', 'users'));
    }

    /**
     * @param IndividualRuleRequest $request
     *
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(IndividualRuleRequest $request)
    {

        $this->authorize('create', IndividualRule::class);

        $rule = $this->repo->create($this->request->all());

        $this->activity->record([
            'module'   => $this->module,
            'module_id' => $rule->id,
            'activity' => 'added'
        ]);

        return $this->success(['message' => trans('individual_rule.added')]);
    }

    /**
     * @param $id
     *
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show($id)
    {
        $rule = $this->repo->findOrFail($id);

        $this->authorize('show', $rule);

        $countries = generateNormalSelectOption(config('country'));

        $users = generateSelectOption($this->user->getUsersIdAndEmail());

        return $this->success(compact('rule','countries', 'users'));
    }

    /**
     * @param $id
     *
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function toggleStatus($id)
    {
        $rule = $this->repo->findOrFail($id);

        $this->authorize('update', $rule);

        $rule = $this->repo->toggle($rule);

        $this->activity->record([
            'module'   => $this->module,
            'module_id' => $rule->id,
            'activity' => 'updated'
        ]);

        return $this->success(['message' => trans('individual_rule.updated'),'individual_rule' => $rule]);
    }

    /**
     * @param                       $id
     * @param IndividualRuleRequest $request
     *
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update($id, IndividualRuleRequest $request)
    {
        $rule = $this->repo->findOrFail($id);

        $this->authorize('update', $rule);

        $rule = $this->repo->update($rule, $this->request->all());

        $this->activity->record([
            'module'   => $this->module,
            'module_id' => $rule->id,
            'activity' => 'updated'
        ]);

        return $this->success(['message' => trans('individual_rule.updated')]);
    }

    /**
     * @param $id
     *
     * @return Response
     * @throws \Exception
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy($id)
    {
        $rule = $this->repo->findOrFail($id);

        $this->authorize('delete', $rule);

        $this->activity->record([
            'module'   => $this->module,
            'module_id' => $rule->id,
            'activity' => 'deleted'
        ]);

        $this->repo->delete($rule);

        return $this->success(['message' => trans('individual_rule.deleted')]);
    }

}
