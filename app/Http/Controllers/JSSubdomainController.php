<?php

namespace App\Http\Controllers;

use App\Exceptions\InvalidDomainException;
use App\Exceptions\InvalidProjectOrRefererException;
use App\Exceptions\InvalidUserBalanceException;
use App\Repositories\ProjectLogRepository;
use App\Repositories\ProjectRepository;
use Illuminate\Http\Request;
use Illuminate\View\View;

/**
 * Class JSSubdomainController
 * @package App\Http\Controllers
 */
class JSSubdomainController extends Controller
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var ProjectRepository
     */
    protected $repo;

    /**
     * @var ProjectLogRepository
     */
    protected $log;

    /**
     * JSSubdomainController constructor.
     *
     * @param Request              $request
     * @param ProjectRepository    $repo
     * @param ProjectLogRepository $log
     */
    public function __construct(Request $request, ProjectRepository $repo, ProjectLogRepository $log)
    {
        $this->request = $request;
        $this->repo    = $repo;
        $this->log     = $log;

    }

    /**
     * Return redirect to main domain
     */
    public function index()
    {
        return redirect(env('APP_URL'));
    }

    /**
     * @return View
     * @throws InvalidProjectOrRefererException
     */
    public function init(): View
    {
        if (!$this->request->header('Referer')) {
            throw new InvalidProjectOrRefererException();
        }

        $domain  = parse_url($this->request->header('Referer'), PHP_URL_HOST);
        $project = $this->repo->findOrFailByDomain($domain);

        if (!$project) {
            return view("javascript.js_init_empty");
        }

        return view("javascript.js_init")->with('project', $project);
    }

    /**
     * @param $id
     *
     * @return $this
     * @throws InvalidDomainException
     * @throws InvalidProjectOrRefererException
     * @throws InvalidUserBalanceException
     */
    public function show($id)
    {
        $project = $this->repo->findOrFailWithAllRules($id);

        if (!$project || !$project->status || !$this->request->header('Referer')) {
            throw new InvalidProjectOrRefererException();
        }

        $domain = parse_url($this->request->header('Referer'), PHP_URL_HOST);

        if ($project->domain != $domain) {
            throw new InvalidDomainException();
        }

        $user = $project->user()->with('profile')->first();

        if ($user->profile->balance < 0.01) {
            throw new InvalidUserBalanceException();
        }

        $user->profile->balance = $user->profile->balance - 0.01;
        $user->save();

        $geo_data = geoip($this->request->ip());

        /*
         * Put record to analytics
         */
        $this->log->record([
            'project_id'         => $project->id,
            'user_id'            => $project->user_id,
            'referer'            => $this->request->header('referer'),
            'is_bot'             => 0,
            'is_moderator'       => 0,
            'iso_country_code'   => $geo_data->iso_code,
            'ip_address'         => $this->request->ip(),
            'keyword'            => '',
            'user_agent'         => $this->request->header('User-Agent'),
            'browser_locale'     => $this->request->header('browserLanguage'),
            'browserName'        => $this->request->header('browserName'),
            'dataCookiesEnabled' => $this->request->header('dataCookiesEnabled'),
            'browserVersion1a'   => $this->request->header('browserVersion1a'),
            'scrPixelDepth'      => $this->request->header('scrPixelDepth'),
            'javaEnabled'        => $this->request->header('javaEnabled'),
            'sizeScreenW'        => $this->request->header('sizeScreenW'),
            'browserOnline'      => $this->request->header('browserOnline'),
            'timeOpened'         => $this->request->header('timeOpened'),
            'browserPlatform'    => $this->request->header('browserPlatform'),
            'timezone_browser'   => $this->request->header('timezone'),
            'sizeAvailH'         => $this->request->header('sizeAvailH'),
            'sizeInW'            => $this->request->header('sizeInW'),
            'sizeInH'            => $this->request->header('sizeInH'),
            'sizeScreenH'        => $this->request->header('sizeScreenH'),
            'pageon'             => $this->request->header('pageon'),
            'browserVersion1b'   => $this->request->header('browserVersion1b'),
            'scrColorDepth'      => $this->request->header('scrColorDepth'),
            'previousSites'      => $this->request->header('previousSites'),
            'sizeAvailW'         => $this->request->header('sizeAvailW'),
            'browserEngine'      => $this->request->header('browserEngine'),
            'country_name'       => $geo_data->country_name,
            'city'               => $geo_data->city,
            'state'              => $geo_data->state,
            'state_name'         => $geo_data->state_name,
            'lat'                => $geo_data->lat,
            'long'               => $geo_data->long,
            'timezone_ip'        => $geo_data->timezone,
            'continent'          => $geo_data->continent,
            'currency'           => $geo_data->currency,
        ]);

        if ($project->individual_rule()->where('status', 1)->count() > 0) {

            $individual_rule = $project->individual_rule()->where('status', 1)->first();
            $countries       = collect(config('country'))->only(collect($individual_rule->country_id))->filter(function (
                $value,
                $key
            ) use ($geo_data) {
                return $value === $geo_data->getAttribute('country');
            });
            $keywords        = collect($individual_rule->keyword_name)->filter(function ($value, $key) {
                foreach ($this->request->all() as $param) {
                    return $value['name'] === $param;
                }
            });

            /*
             * Country not whitelisted
             */
            if ($individual_rule->country_id && !$countries->count()) {
                if ($project->destinations && $project->bad_destination) {
                    return responseJSON($project->bad_destination);
                } else {
                    return responseJSON('http://127.0.0.1');
                }
            }

            /*
             * Keyword in blocklist
             */
            if ($keywords->count()) {
                if ($project->destinations && $project->bad_destination) {
                    return responseJSON($project->bad_destination);
                } else {
                    return responseJSON('http://127.0.0.1');
                }
            }

            if ($project->destinations && $project->good_destination) {
                return responseJSON($project->good_destination);
            } else {
                return responseJSON();
            }

        } elseif ($project->rules()->where('status', 1)->count() > 0) {

            $rules             = $project->rules()->where('status', 1)->get();
            $keyword_blacklist = false;
            $country_blacklist = false;

            foreach ($rules as $rule) {
                $countries = collect(config('country'))->only(collect($rule->country_id))->filter(function (
                    $value,
                    $key
                ) use ($geo_data) {
                    return $value === $geo_data->getAttribute('country');
                });
                $keywords  = collect($rule->keyword_name)->filter(function ($value, $key) {
                    foreach ($this->request->all() as $param) {
                        return $value['name'] === $param;
                    }
                });

                /*
                * Country not whitelisted
                */
                if ($rule->country_id && !$countries->count()) {
                    $country_blacklist = true;
                }

                /*
                 * Keyword in blocklist
                 */
                if ($keywords->count()) {
                    $keyword_blacklist = true;
                }
            }

            if ($keyword_blacklist || $country_blacklist) {
                if ($project->destinations && $project->bad_destination) {
                    return responseJSON($project->bad_destination);
                } else {
                    return responseJSON('http://127.0.0.1');
                }
            }

            if ($project->destinations && $project->good_destination) {
                return responseJSON($project->good_destination);
            } else {
                return responseJSON();
            }

        } else {
            return responseJSON();
        }

    }

}
