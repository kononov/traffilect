<?php

namespace App\Http\Controllers;

use App\Http\Requests\RuleRequest;
use App\Repositories\ActivityLogRepository;
use App\Repositories\ProjectRepository;
use App\Repositories\RuleRepository;
use App\Rule;
use Illuminate\Http\Request;

class RuleController extends Controller
{
    /**
     * @var string
     */
    protected $module = 'rule';

    /**
     * @var Request
     */
    private $request;

    /**
     * @var ProjectRepository
     */
    private $repo;

    /**
     * @var ActivityLogRepository
     */
    protected $activity;

    /**
     * @var ProjectRepository
     */
    protected $project;

    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */

    public function __construct(Request $request, RuleRepository $repo, ActivityLogRepository $activity, ProjectRepository $project)
    {
        $this->request  = $request;
        $this->repo     = $repo;
        $this->activity = $activity;
        $this->project = $project;

        $this->middleware('feature.available:rule');
    }

    /**
     * @return mixed
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */

    public function index()
    {
        $this->authorize('view', Rule::class);

        return $this->repo->paginate($this->request->all());
    }

    /**
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function preRequisite()
    {
        $this->authorize('create', Rule::class);

        $countries = generateNormalSelectOption(config('country'));
        $projects = generateSelectOption($this->project->listExceptName());

        return $this->success(compact('countries', 'projects'));
    }

    /**
     * @param RuleRequest $request
     *
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(RuleRequest $request)
    {

        $this->authorize('create', Rule::class);

        $rule = $this->repo->create($this->request->all());

        $this->activity->record([
            'module'   => $this->module,
            'module_id' => $rule->id,
            'activity' => 'added'
        ]);

        return $this->success(['message' => trans('rule.added')]);
    }

    /**
     * @param $id
     *
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show($id)
    {
        $rule = $this->repo->findOrFail($id);

        $this->authorize('show', $rule);

        $countries = generateNormalSelectOption(config('country'));
        $projects = generateSelectOption($this->project->listExceptName());

        return $this->success(compact('rule','countries', 'projects'));
    }

    /**
     * @param $id
     *
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function toggleStatus($id)
    {
        $rule = $this->repo->findOrFail($id);

        $this->authorize('update', $rule);

        $rule = $this->repo->toggle($rule);

        $this->activity->record([
            'module'   => $this->module,
            'module_id' => $rule->id,
            'activity' => 'updated'
        ]);

        return $this->success(['message' => trans('rule.updated'),'rule' => $rule]);
    }

    /**
     * @param             $id
     * @param RuleRequest $request
     *
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update($id, RuleRequest $request)
    {
        $rule = $this->repo->findOrFail($id);

        $this->authorize('update', $rule);

        $rule = $this->repo->update($rule, $this->request->all());

        $this->activity->record([
            'module'   => $this->module,
            'module_id' => $rule->id,
            'activity' => 'updated'
        ]);

        return $this->success(['message' => trans('rule.updated')]);
    }

    /**
     * @param $id
     *
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy($id)
    {
        $rule = $this->repo->findOrFail($id);

        $this->authorize('delete', $rule);

        $this->activity->record([
            'module'   => $this->module,
            'module_id' => $rule->id,
            'activity' => 'deleted'
        ]);

        $this->repo->delete($rule);

        return $this->success(['message' => trans('rule.deleted')]);
    }

}
