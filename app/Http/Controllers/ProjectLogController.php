<?php

namespace App\Http\Controllers;

use App\Repositories\ProjectLogRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;

class ProjectLogController extends Controller
{
    protected $module = 'project_log';
    protected $request;
    protected $repo;
    protected $user;

    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */

    public function __construct(Request $request, ProjectLogRepository $repo, UserRepository $user)
    {
        $this->request = $request;
        $this->repo    = $repo;
        $this->user    = $user;

        $this->middleware('feature.available:project');
    }

    /**
     * @return Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index()
    {
        $project_logs = $this->repo->paginate($this->request->all());

        $projects = $this->user->findOrFail(\Auth::user()->id)->projects()->getResults();

        return $this->success(compact('projects', 'project_logs'));
    }
}
