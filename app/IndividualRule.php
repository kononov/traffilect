<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IndividualRule extends Model
{
    /**
     * @var array
     */
    protected $fillable = [];

    /**
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var string
     */
    protected $table = 'individual_rules';

    /**
     * @var array
     */
    protected $casts = [
        'country_id' => 'json',
        'keyword_name' => 'json',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function projects()
    {
        return $this->belongsToMany('App\Project')->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('App\User', 'individual_rule_user')->withTimestamps();
    }

    /**
     * @param      $q
     * @param null $user_id
     *
     * @return mixed
     */
    public function scopeFilterByUserId($q, $user_id = null)
    {
        if (! $user_id) {
            return $q;
        }

        return $q->whereUserId($user_id);
    }

    /**
     * @param      $q
     * @param null $keyword
     *
     * @return mixed
     */
    public function scopeFilterByTitleOrDescription($q, $keyword = null)
    {
        if (! $keyword) {
            return $q;
        }

        return $q->where('title', 'like', '%'.$keyword.'%')->orWhere('description', 'like', '%'.$keyword.'%');
    }

    /**
     * @param      $q
     * @param null $status
     *
     * @return mixed
     */
    public function scopeFilterCompletedRule($q, $status = null)
    {
        if (! $status) {
            return $q;
        }

        return $q->whereStatus(1);
    }

    /**
     * @param $q
     * @param $dates
     *
     * @return mixed
     */
    public function scopeDateBetween($q, $dates)
    {
        if ((! $dates['start_date'] || ! $dates['end_date']) && $dates['start_date'] <= $dates['end_date']) {
            return $q;
        }

        return $q->where('date', '>=', getStartOfDate($dates['start_date']))->where('date', '<=', getEndOfDate($dates['end_date']));
    }
}
