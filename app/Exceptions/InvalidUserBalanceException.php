<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\Response;

/**
 * Class InvalidUserBalanceException
 * @package App\Exceptions
 */
class InvalidUserBalanceException extends Exception
{
    /**
     * @return Response
     */
    public function render(): Response
    {
        return response('Invalid user balance', 403)->header('Content-Type', 'text/plain');
    }
}
