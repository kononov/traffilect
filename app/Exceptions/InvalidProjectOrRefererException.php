<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\Response;

/**
 * Class InvalidProjectOrRefererException
 * @package App\Exceptions
 */
class InvalidProjectOrRefererException extends Exception
{
    /**
     * @return Response
     */
    public function render(): Response
    {
        return response('Invalid project', 403)->header('Content-Type', 'text/plain');
    }
}
