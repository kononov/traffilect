<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\Response;

/**
 * Class InvalidDomainException
 * @package App\Exceptions
 */
class InvalidDomainException extends Exception
{
    /**
     * @return Response
     */
    public function render(): Response
    {
        return response('Invalid domain name', 403)->header('Content-Type', 'text/plain');
    }
}
