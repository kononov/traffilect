<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class DepositCurrency
 * @package App
 */
class DepositCurrency extends Model
{
    protected $fillable = [];

    /**
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var string
     */
    protected $table = 'deposit_currencies';

    /**
     * @return mixed
     */
    public function deposit()
    {
        return $this->belongsTo('App\Deposit');
    }


}
