<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['individual_rule_id'];

    /**
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var string
     */
    protected $table = 'projects';

    /**
     * @var
     */
    protected $rule;

    /**
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function individual_rule()
    {
        return $this->belongsTo('App\IndividualRule');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ProjectLog()
    {
        return $this->hasMany('App\ProjectLog');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function rules()
    {
        return $this->belongsToMany('App\Rule', 'project_rule')->withTimestamps();
    }

    /**
     * @param $q
     * @param null $keyword
     * @return mixed
     */
    public function scopeFilterByTitleOrDescription($q, $keyword = null)
    {
        if (! $keyword) {
            return $q;
        }

        return $q->where('title', 'like', '%'.$keyword.'%')->orWhere('description', 'like', '%'.$keyword.'%');
    }

    /**
     * @param      $q
     * @param null $status
     *
     * @return mixed
     */
    public function scopeFilterCompletedProject($q, $status = null)
    {
        if (! $status) {
            return $q;
        }

        return $q->whereStatus(1);
    }

    /**
     * @param      $q
     * @param null $user_id
     *
     * @return mixed
     */
    public function scopeFilterByUserId($q, $user_id = null)
    {
        if (! $user_id) {
            return $q;
        }

        return $q->whereUserId($user_id);
    }

    /**
     * @param $q
     * @param $dates
     *
     * @return mixed
     */
    public function scopeDateBetween($q, $dates)
    {
        if ((! $dates['start_date'] || ! $dates['end_date']) && $dates['start_date'] <= $dates['end_date']) {
            return $q;
        }

        return $q->where('date', '>=', getStartOfDate($dates['start_date']))->where('date', '<=', getEndOfDate($dates['end_date']));
    }

    public function setRule()
    {
        return json_decode($this->rule, true);
    }

}
