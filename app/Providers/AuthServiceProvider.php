<?php

namespace App\Providers;

use App\Configuration;
use App\Deposit;
use App\EmailLog;
use App\EmailTemplate;
use App\IndividualRule;
use App\IpFilter;
use App\Locale;
use App\Message;
use App\Permission;
use App\Policies\ConfigurationPolicy;
use App\Policies\DepositPolicy;
use App\Policies\EmailLogPolicy;
use App\Policies\EmailTemplatePolicy;
use App\Policies\IndividualRulePolicy;
use App\Policies\IpFilterPolicy;
use App\Policies\LocalePolicy;
use App\Policies\MessagePolicy;
use App\Policies\PermissionPolicy;
use App\Policies\ProjectPolicy;
use App\Policies\RolePolicy;
use App\Policies\RulePolicy;
use App\Policies\TodoPolicy;
use App\Policies\UserPolicy;
use App\Project;
use App\Role;
use App\Rule;
use App\Todo;
use App\User;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        Todo::class => TodoPolicy::class,
        Configuration::class => ConfigurationPolicy::class,
        EmailLog::class => EmailLogPolicy::class,
        EmailTemplate::class => EmailTemplatePolicy::class,
        IpFilter::class => IpFilterPolicy::class,
        Locale::class => LocalePolicy::class,
        Message::class => MessagePolicy::class,
        Permission::class => PermissionPolicy::class,
        Role::class => RolePolicy::class,
        User::class => UserPolicy::class,
        Project::class => ProjectPolicy::class,
        Rule::class => RulePolicy::class,
        Deposit::class => DepositPolicy::class,
        IndividualRule::class =>IndividualRulePolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
