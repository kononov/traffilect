<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectLog extends Model
{
    /**
     * @var array
     */
    protected $fillable = [];

    /**
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var string
     */
    protected $table = 'project_logs';

    /**
     * @return mixed
     */
    public function project()
    {
        return $this->belongsTo('App\Project');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * @param      $q
     * @param null $project_id
     *
     * @return mixed
     */
    public function scopeFilterByProjectId($q, $project_id = null)
    {
        if (! $project_id) {
            return $q;
        }

        return $q->whereProjectId($project_id);
    }

    /**
     * @param      $q
     * @param null $user_id
     *
     * @return mixed
     */
    public function scopeFilterByUserId($q, $user_id = null)
    {
        if (! $user_id) {
            return $q;
        }

        return $q->whereUserId($user_id);
    }

    /**
     * @param      $q
     * @param null $keyword
     *
     * @return mixed
     */
    public function scopeFilterByKeyword($q, $keyword = null)
    {
        if (! $keyword) {
            return $q;
        }

        return $q->where('keyword', 'like', '%'.$keyword.'%');
    }

    /**
     * @param $q
     * @param $dates
     *
     * @return mixed
     */
    public function scopeDateBetween($q, $dates)
    {
        if ((! $dates['start_date'] || ! $dates['end_date']) && $dates['start_date'] <= $dates['end_date']) {
            return $q;
        }

        return $q->where('date', '>=', getStartOfDate($dates['start_date']))->where('date', '<=', getEndOfDate($dates['end_date']));
    }


}
