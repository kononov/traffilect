<?php

namespace App\Policies;

use App\User;
use App\Project;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProjectPolicy
{
    use HandlesAuthorization;

    /**
     * @param $user
     * @param $ability
     * @return mixed
     */
    public function before($user, $ability)
    {
        return $user->can('access-project');
    }

    /**
     * @param User $user
     * @return bool
     */
    public function view(User $user)
    {
        return true;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * @param User $user
     * @param Project $project
     * @return bool
     */
    public function show(User $user, Project $project)
    {
        return $project->user_id == $user->id;
    }

    /**
     * @param User $user
     * @param Project $project
     * @return bool
     */
    public function update(User $user, Project $project)
    {
        return $project->user_id == $user->id;
    }

    /**
     * @param User $user
     * @param Project $project
     * @return bool
     */
    public function delete(User $user,  Project $project)
    {
        return $project->user_id == $user->id;
    }
}
