<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

/**
 * Class IndividualRule
 * @package App\Policies
 */
class IndividualRulePolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     *
     * @return bool
     */
    public function before(User $user)
    {
        return $user->can('access-individual-rule');
    }

    /**
     * @param User $user
     *
     * @return bool
     */
    public function view(User $user)
    {
        return true;
    }

    /**
     * @param User $user
     *
     * @return bool
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * @param User           $user
     * @param IndividualRule $rule
     *
     * @return bool
     */
    public function show(User $user, IndividualRule $rule)
    {
        return $rule->user_id == $user->id;
    }

    /**
     * @param User           $user
     * @param IndividualRule $rule
     *
     * @return bool
     */
    public function update(User $user, IndividualRule $rule)
    {
        return $rule->user_id == $user->id;
    }

    /**
     * @param User           $user
     * @param IndividualRule $rule
     *
     * @return bool
     */
    public function delete(User $user, IndividualRule $rule)
    {
        return $rule->user_id == $user->id;
    }
}
