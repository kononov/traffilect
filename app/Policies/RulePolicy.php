<?php

namespace App\Policies;

use App\Rule;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

/**
 * Class RulePolicy
 * @package App\Policies
 */
class RulePolicy
{
    use HandlesAuthorization;

    /**
     * @param $user
     *
     * @return mixed
     */
    public function before($user)
    {
        return $user->can('access-rule');
    }

    /**
     * @param User $user
     *
     * @return bool
     */
    public function view(User $user)
    {
        return true;
    }

    /**
     * @param User $user
     *
     * @return bool
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * @param User $user
     * @param Rule $rule
     *
     * @return bool
     */
    public function show(User $user, Rule $rule)
    {
        return $rule->user_id == $user->id;
    }

    /**
     * @param User $user
     * @param Rule $rule
     *
     * @return bool
     */
    public function update(User $user, Rule $rule)
    {
        return $rule->user_id == $user->id;
    }

    /**
     * @param User $user
     * @param Rule $rule
     *
     * @return bool
     */
    public function delete(User $user, Rule $rule)
    {
        return $rule->user_id == $user->id;
    }
}
