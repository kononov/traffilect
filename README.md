## Traffilect.com site

Private repository for trafillect.com project.
Web application with Laravel 5.6 (backend) and Vue.JS (frontend).

## Clone repository
```html
git clone git@gitlab.com/kononov/traffilect.git
```

## Run docker containers on local

```html
docker-compose up -d
```
After building need add to /etc/hosts:
```html
127.0.0.1   traffilect.loc
```


## Use local environment

```html
http://traffilect.loc
```