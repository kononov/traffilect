<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

/**
 * Class AddDestinationsToProject
 */
class AddDestinationsToProject extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('projects', function($table) {
            $table->string('bad_destination',200)->nullable();
            $table->string('good_destination',200)->nullable();
            $table->integer('destinations')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('projects', function($table) {
            $table->dropColumn('bad_destination');
            $table->dropColumn('good_destination');
            $table->dropColumn('destinations');
        });
    }
}
