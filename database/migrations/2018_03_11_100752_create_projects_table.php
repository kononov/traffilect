<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateProjectsTable
 */
class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {

            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('title', 50)->nullable();
            $table->text('description')->nullabe();
            $table->boolean('status')->default(0);
            $table->dateTime('completed_at')->nullable();
            $table->integer('block_robots')->default(0);
            $table->integer('block_moderators')->default(0);
            $table->BigInteger('total_visitors')->default(0);
            $table->BigInteger('unique_visitors')->default(0);
            $table->BigInteger('bot_counter')->default(0);
            $table->BigInteger('moderator_counter')->default(0);
            $table->timestamps();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
