<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateProjectLogsTable
 */
class CreateProjectLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->text('referer')->nullable();
            $table->integer('is_bot')->default(0);
            $table->integer('is_moderator')->default(0);
            $table->string('iso_country_code', 2)->nullable();
            $table->ipAddress('ip_address')->nullable();
            $table->string('keyword', 50)->nullable();
            $table->string('country_name', 30)->nullable();
            $table->string('city', 30)->nullable();
            $table->string('state', 30)->nullable();
            $table->string('state_name', 30)->nullable();
            $table->decimal('lat', 10, 7);
            $table->decimal('long', 10, 7);
            $table->string('timezone_browser', 30)->nullable();
            $table->string('timezone_ip', 30)->nullable();
            $table->string('continent', 5)->nullable();
            $table->string('currency', 5)->nullable();
            $table->string('user_agent', 255)->nullable();
            $table->string('browserName', 50)->nullable();
            $table->boolean('dataCookiesEnabled');
            $table->string('browserVersion1a', 255)->nullable();
            $table->integer('scrPixelDepth');
            $table->boolean('javaEnabled');
            $table->integer('sizeScreenW');
            $table->boolean('browserOnline');
            $table->string('timeOpened', 50)->nullable();
            $table->string('browserPlatform', 50)->nullable();
            $table->integer('sizeAvailH');
            $table->integer('sizeInW');
            $table->integer('sizeInH');
            $table->integer('sizeScreenH');
            $table->string('pageon', 500)->nullable();
            $table->string('browserVersion1b', 255)->nullable();
            $table->integer('scrColorDepth');
            $table->string('browser_locale', 15)->nullable();
            $table->integer('previousSites');
            $table->integer('sizeAvailW');
            $table->string('browserEngine', 20)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_logs');
    }
}
