<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class ProjectLogsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('project_logs')->insert([
            'id'                 => 1,
            'project_id'         => 1,
            'user_id'            => 1,
            'referer'            => 'http://google.ru',
            'is_bot'             => 1,
            'is_moderator'       => 0,
            'iso_country_code'   => 'RU',
            'ip_address'         => '13.19.190.11',
            'keyword'            => 'test',
            'user_agent'         => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.1 Safari/605.1.15',
            'browser_locale'     => 'ru-RU',
            'created_at'         => Carbon::now()->format('Y-m-d H:i:s'),
            'browserName'        => 'Netscape',
            'dataCookiesEnabled' => true,
            'browserVersion1a'   => '5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.1 Safari/605.1.15',
            'scrPixelDepth'      => 24,
            'javaEnabled'        => true,
            'sizeScreenW'        => 1440,
            'browserOnline'      => true,
            'timeOpened'         => 'Sun Apr 08 2018 02:23:19 GMT+0300 (MSK)',
            'browserPlatform'    => 'MacIntel',
            'timezone_browser'   => '-3',
            'sizeAvailH'         => 826,
            'sizeInW'            => 1440,
            'sizeInH'            => 418,
            'sizeScreenH'        => 900,
            'pageon'             => '/catalog/',
            'browserVersion1b'   => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.1 Safari/605.1.15',
            'scrColorDepth'      => 24,
            'previousSites'      => 3,
            'sizeAvailW'         => 1440,
            'browserEngine'      => 'Gecko',
            'country_name'       => 'Russia',
            'city'               => 'Kirishi',
            'state'              => 'LEN',
            'state_name'         => 'Leningradskaya Oblast\'',
            'lat'                => 59.45,
            'long'               => 32.0211,
            'timezone_ip'        => 'Europe/Moscow',
            'continent'          => 'EU',
            'currency'           => 'RUB',
        ]);

    }
}
