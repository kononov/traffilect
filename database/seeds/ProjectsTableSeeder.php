<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class ProjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('projects')->insert([
            'id' => 1,
            'user_id' => 1,
            'title' => 'Test project',
            'description' => 'Test project for traffilect.com',
            'status' => 1,
            'block_robots' => 1,
            'block_moderators' => 1,
            'total_visitors' => 15,
            'unique_visitors' => 10,
            'bot_counter' => 4,
            'moderator_counter' => 5,
            'domain' => 'traffilect.com',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
