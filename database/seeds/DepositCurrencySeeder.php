<?php

use Illuminate\Database\Seeder;

/**
 * Class DepositCurrencySeeder
 */
class DepositCurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('deposit_currencies')->insert([
            'id'   => 1,
            'name' => 'BTC',
        ]);
    }
}
