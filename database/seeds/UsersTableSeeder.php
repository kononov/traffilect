<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id'         => 1,
            'email'      => 'info@kononov.spb.ru',
            'password'   => bcrypt('c3XZZtuv'),
            'status'     => 'activated',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
