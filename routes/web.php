<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
 * js subdomain for sites filtering
 */
Route::group(['middleware' => 'cors', 'domain' => 'js.traffilect.com', 'https' => true], function () {
    Route::get('/', 'JSSubdomainController@index');
    Route::get('/init', 'JSSubdomainController@init');
    Route::get('/{id}', 'JSSubdomainController@show');
});

/*
 * api subdomain for sites filtering
 */
Route::group(array('domain' => 'api.traffilect.com'), function () {
    Route::get('/', 'HomeController@test');
});

Route::group(['middleware' => ['jwt.auth']], function () {
    Route::get('/backup/{id}/download', 'BackupController@download');
    Route::get('/message/{message_uuid}/attachment/{attachment_uuid}/download', 'MessageController@download');
});

Route::get('/auth/social/{provider}', 'SocialLoginController@providerRedirect');
Route::get('/auth/{provider}/callback', 'SocialLoginController@providerRedirectCallback');

/*
 * Used to get translation in json format for current locale
 */
Route::get('/js/lang', function () {
    if (App::environment('local')) {
        Cache::forget('lang.js');
    }
    $strings = Cache::rememberForever('lang.js', function () {
        $lang    = config('app.locale');
        $files   = glob(resource_path('lang/' . $lang . '/*.php'));
        $strings = [];
        foreach ($files as $file) {
            $name           = basename($file, '.php');
            $strings[$name] = require $file;
        }

        return $strings;
    });
    header('Content-Type: text/javascript');
    echo('window.i18n = ' . json_encode($strings) . ';');
    exit();
})->name('assets.lang');

/*
 * VueJS routes
 */
Route::get('/{vue?}', function () {
    return view('home');
})->where('vue', '[\/\w\.-]*')->name('home');
